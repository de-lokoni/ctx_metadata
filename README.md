# Metadata To be used for testing commits and promotions
This is a list of sample metadata which can be used as a baseline for testing.

# Contents
Objects, Record Types, Picklist with Record Type and Picklist Dependencies, Programatic components, Profiles, Perm Sets Translations & Others. See full list below

# prerequisites
* Enable Translations on the org to which you need to deploy the changes
* Languages are English, Spanish, Japanese, Traditional Chinese

# Installation Instructions
Unfortunately I was not able to package it.
However, to get it onto an org do the following:
* Configure your SFDX CLI. Use Aliases for org names to make your live easier. Check google for more help. (sfdx setup cli trailhead /getting started with salesforce dx)
* connect it to your dev hub   
    * e.g. sfdx auth:login:web -d -a myDevHubAlias
* Connect it to your "dev" org in your pipeline
    * e.g. sfdx auth:login:web -a myDev1Blitz

Next you need to connect your CLI to a project.
If you are fancy and you know what you are doing, clone the repo and post-fit it to your local requirements.
If you are a noob, don't panic.  
* Use your CLI to create a new empty project in folder A on your machine (e.g. laptop). This will create some files, amongst others the config folder, the sfdx-config.json, the package.json and the sfdx-project.json

* Clone the repository of this metadata into folder B. 
    * ssh clone command: git clone git@bitbucket.org:de-lokoni/ctx_metadata.git  
    * https clone command: git clone https://de-lokoni@bitbucket.org/de-lokoni/ctx_metadata.git  
* Copy the project-json and CTX_Metadata folder from folder B and copy it in folder A. Overwrite existing files.
* Remove the force-app folder
* Open VSCode (or any editor or CLI) in Folder A. 
* Execute the source:deploy command to deploy the metadata onto your org:  
    * e.g. sfdx force:source:deploy -u myOrgAlias -p CTX_Metadata    

You can find a video description here:  
https://drive.google.com/file/d/1vqQKDhU9eOlGYaTJxyc7FX1BfEbHYycS/view?usp=sharing

